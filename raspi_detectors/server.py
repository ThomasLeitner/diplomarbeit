#AUTHOR: LEITNER THOMAS & JACOB JORDAN  @HTL Anichstraße Innsbbruck


#!/usr/bin/env python
#import os

from SimpleXMLRPCServer import SimpleXMLRPCServer
from time import sleep

from SimpleXMLRPCServer import SimpleXMLRPCRequestHandler
import RPi.GPIO as GPIO

from functools import partial




class RequestHandler(SimpleXMLRPCRequestHandler):

    rpc_paths = ('/RPC2',)




#server = SimpleXMLRPCServer(("localhost", 8000),

#IPAdresse = os.popen('hostname -I').readlines()
#print(IPAdresse)
server = SimpleXMLRPCServer(("localhost", 8000),
				requestHandler=RequestHandler)

server.register_introspection_functions()

GPIO.setmode(GPIO.BCM)
d1 = 4	#Schleifen nummerierung anhand den GPIO Pins
d2 = 17
d3 = 26
d4 = 13
d5 = 22
d6 = 5

GPIO.setup(d1, GPIO.IN)
GPIO.setup(d2, GPIO.IN)
GPIO.setup(d3, GPIO.IN)
GPIO.setup(d4, GPIO.IN)
GPIO.setup(d5, GPIO.IN)
GPIO.setup(d6, GPIO.IN)


sector1=0
sector2=0
sector3=0
sector4=0
sector5=0
sector6=0

# Create a function to run when the input is high
def detected(detector):
    global sector1, sector2, sector3, sector4, sector5, sector6
    if detector == 1 :
        sector1 +=1
        if sector6 > 0:
          sector6 -=1
    if detector == 2 :
        sector2 +=1
        if sector1 > 0:
          sector1 -=1
    if detector == 3 :
        sector3 +=1
        if sector2 > 0:
          sector2 -=1
    if detector == 4 :
        sector4 +=1
        if sector3 > 0:
          sector3 -=1
    if detector == 5 :
        sector5 +=1
        if sector4 > 0:
          sector4 -=1
    if detector == 6 :
        sector6 +=1
        if sector5 > 0:
          sector5 -=1
    print("Detector" +str( detector))

GPIO.add_event_detect(d1,GPIO.RISING, callback=lambda x: detected(1), bouncetime=200)
GPIO.add_event_detect(d2,GPIO.RISING, callback=lambda x: detected(2), bouncetime=200)
GPIO.add_event_detect(d3,GPIO.RISING, callback=lambda x: detected(3), bouncetime=200)
GPIO.add_event_detect(d4,GPIO.RISING, callback=lambda x: detected(4), bouncetime=200)
GPIO.add_event_detect(d5,GPIO.RISING, callback=lambda x: detected(5),  bouncetime=200)
GPIO.add_event_detect(d6,GPIO.RISING, callback=lambda x: detected(6), bouncetime=200)


# Start a loop that never ends
while True:
    print("================================")
    print("Number of robots in 1 " + str(sector1))
    print("Number of robots in 2 " + str(sector2))
    print("Number of robots in 3 " + str(sector3))
    print("Number of robots in 4 " + str(sector4))
    print("Number of robots in 5 " + str(sector5))
    print("Number of robots in 6 " + str(sector6))
    sleep(1)           # Sleep for a full second before restarting our loop

server.serve_forever()



