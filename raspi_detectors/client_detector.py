#AUTHOR: LEITNER THOMAS & JACOB JORDAN  @HTL Anichstraße Innsbbruck

import rpyc
import os
import time
import RPi.GPIO as GPIO
from time import sleep
from functools import partial

GPIO.setmode(GPIO.BCM)
# Pinbelegung fuer die Induktionsschleifen
d1 = 4
d2 = 17
d3 = 26
d4 = 13
d5 = 22
d6 = 5

# Setup der GPIO Pins als Input
GPIO.setup(d1, GPIO.IN)
GPIO.setup(d2, GPIO.IN)
GPIO.setup(d3, GPIO.IN)
GPIO.setup(d4, GPIO.IN)
GPIO.setup(d5, GPIO.IN)
GPIO.setup(d6, GPIO.IN)

# Initialisierung der Sectoren der Arena
sectors = [0] * 6


def detected(detector):
    global sectors
    # Da das erste Element im Array 0 ist
    sectors[detector-1] += 1
    # Vorhergehender Sector im Array 
    if detector==1:
       detector=7
    if sectors[detector-2] > 0:
       sectors[detector-2] -= 1
       print("Decrease"+ str( detector-2))


GPIO.add_event_detect(d1,GPIO.RISING, callback=lambda x: detected(1), bouncetime=200)
GPIO.add_event_detect(d2,GPIO.RISING, callback=lambda x: detected(2), bouncetime=200)
GPIO.add_event_detect(d3,GPIO.RISING, callback=lambda x: detected(3), bouncetime=200)
GPIO.add_event_detect(d4,GPIO.RISING, callback=lambda x: detected(4), bouncetime=200)
GPIO.add_event_detect(d5,GPIO.RISING, callback=lambda x: detected(5), bouncetime=200)
GPIO.add_event_detect(d6,GPIO.RISING, callback=lambda x: detected(6), bouncetime=200)

# ToDo: Automatisches Suchen nach eingeschaltenen Robotoren
r1=rpyc.connect("192.168.1.103", 18812)
r2=rpyc.connect("192.168.1.102", 18812)
r3=rpyc.connect("192.168.1.103", 18812)
r4=rpyc.connect("192.168.1.104", 18812)
r5=rpyc.connect("192.168.1.105", 18812)

robo1= r1.root.AutonomousCarServiceClass()
robo2= r2.root.AutonomousCarServiceClass()
robo3= r3.root.AutonomousCarServiceClass()
robo4= r4.root.AutonomousCarServiceClass()
robo5= r5.root.AutonomousCarServiceClass()

myrobots = [robo1,robo2,robo3,robo4,robo5]

for robot in myrobots:
  robot.readVoltage()
  robot.readCapacity()
  robot.startme()  

while True:
  for robot in myrobots:
    robot.runme()
    if(all(sector <= 1 for sector in sectors)):
      robot.inc_max_speed()
      print( "Increase") 
    if(any(sector > 1 for sector in sectors)):
      robot.dec_max_speed()
      print( "Decrease") 

  print("================================")
  print("Number of robots in 1 " + str(sectors[0]))
  print("Number of robots in 2 " + str(sectors[1]))
  print("Number of robots in 3 " + str(sectors[2]))
  print("Number of robots in 4 " + str(sectors[3]))
  print("Number of robots in 5 " + str(sectors[4]))
  print("Number of robots in 6 " + str(sectors[5]))
  #sleep(0.1)           # Sleep for a full second before restarting our loop


