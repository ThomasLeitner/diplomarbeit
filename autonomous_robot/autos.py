#AUTHOR: LEITNER THOMAS & JACOB JORDAN  @HTL Anichstraße Innsbbruck

import RPi.GPIO as GPIO
import time
import os #für IP Addr.
import struct
import smbus
import sys
import time
import datetime
from demo_opts import get_device
from luma.core.render import canvas
import os.path
from PIL import Image, ImageSequence
from luma.core.sprite_system import framerate_regulator

direction = 18
speed = 13
GPIO_TRIGGER = 23
GPIO_ECHO = 24
GPIO_RED = 16
GPIO_GREEN = 20

GPIO.setmode(GPIO.BCM)

GPIO.setup(GPIO_TRIGGER, GPIO.OUT)
GPIO.setup(GPIO_ECHO, GPIO.IN)
GPIO.setup(speed, GPIO.OUT)
GPIO.setup(GPIO_RED, GPIO.OUT)
GPIO.setup(GPIO_GREEN, GPIO.OUT)

pwm_speed = GPIO.PWM(speed, 100)


def distanz():
    GPIO.output(GPIO_TRIGGER, True)
    # setze Trigger nach 0.01ms aus LOW
    time.sleep(0.00001)
    GPIO.output(GPIO_TRIGGER, False)

    StartZeit = time.time()
    StopZeit = time.time()
 
    # speichere Startzeit
    while GPIO.input(GPIO_ECHO) == 0:
        StartZeit = time.time()
 
    # speichere Ankunftszeit
    while GPIO.input(GPIO_ECHO) == 1:
        StopZeit = time.time()
 
    # Zeit Differenz zwischen Start und Ankunft
    TimeElapsed = StopZeit - StartZeit
    # mit der Schallgeschwindigkeit (34300 cm/s) multiplizieren
    # und durch 2 teilen, da hin und zurueck
    distanz = (TimeElapsed * 34300) / 2
 
    return distanz

def readVoltage(bus):
    address = 0x36
    read = bus.read_word_data(address, 2)
    swapped = struct.unpack("<H", struct.pack(">H", read))[0]
    voltage = swapped * 78.125 /1000000
    return voltage

def readCapacity(bus):
    address = 0x36
    read = bus.read_word_data(address, 4)
    swapped = struct.unpack("<H", struct.pack(">H", read))[0]
    capacity = swapped/256
    return capacity

def primitives(device, draw):
    # First define some constants to allow easy resizing of shapes.
    padding = 2
    shape_width = 20
    top = padding
    bottom = device.height - padding - 1
    # Move left to right keeping track of the current x position for drawing shapes.
    x = padding
    # Write two lines of text.
    size = draw.textsize('World!')
    x = device.width - padding - size[0]
    draw.text((0, top + 4), 'Voltage:%5.2fV:  ' % readVoltage(bus), fill="white")
    draw.text((0, top + 16), 'Battery:%5i%%' % readCapacity(bus), fill="white")
    draw.text((0, top + 28), 'Abstand:%5.2fcm' % abstand, fill="white")

def animation():
    regulator = framerate_regulator(fps=10)
    img_path = os.path.abspath(os.path.join(os.path.dirname(__file__),
        'images', 'banana.gif'))
    banana = Image.open(img_path)
    size = [min(*device.size)] * 2
    posn = ((device.width - size[0]) // 2, device.height - size[1])

    while True:
        for frame in ImageSequence.Iterator(banana):
            with regulator:
                background = Image.new("RGB", device.size, "white")
                background.paste(frame.resize(size, resample=Image.LANCZOS), posn)
                device.display(background.convert(device.mode))
    
try:
    IPAdresse = os.popen('hostname -I').readlines()
    print("Your IP-Adress is " + " ".join(IPAdresse) + ".")
    GPIO.output(GPIO_RED, GPIO.HIGH)
    GPIO.output(GPIO_GREEN, GPIO.HIGH)
    wtd = input("Press d to drive\nPress a to show a animation\nPress any Key to show status of Battery")
    if wtd == 'd':
        pwm_speed.start(70)
        running = True
        print("goo")
        while True:
            bus = smbus.SMBus(1)
            abstand = distanz()
            print ("Gemessene Entfernung = %.1f cm" % abstand)
            if abstand <= 60:
                pwm_speed.stop()
                GPIO.output(speed, GPIO.LOW)
                running = False
            if abstand >= 60 and running == False:
                pwm_speed.start(70)
            time.sleep(0.5)
    if wtd == 'a':
        device = get_device()
        animation()
    else:
        while(True):
            bus = smbus.SMBus(1)
            abstand = distanz()
            device = get_device()
            with canvas(device) as draw:
                primitives(device, draw)
            time.sleep(2)

finally:
    print("\n Clean up - set GPIOs to inout mode \n")
    pwm_speed.stop()
    GPIO.output(speed, GPIO.LOW)
    GPIO.cleanup()



