#AUTHOR: LEITNER THOMAS & JACOB JORDAN  @HTL Anichstraße Innsbbruck

import rpyc
from rpyc.utils.server import ThreadedServer
import RPi.GPIO as GPIO
import os #für IP Addr.
import struct
import smbus
import sys
import time
import datetime
from demo_opts import get_device
from PIL import Image, ImageSequence
from luma.core.render import canvas
from luma.core.sprite_system import framerate_regulator
import threading
from luma.oled.device import sh1106
from luma.core.interface.serial import i2c
import atexit
import timeit


class AutonomousCarService(rpyc.Service):
    def __init__(self):
        print("Robot Object erstellt")
        self.bus = smbus.SMBus(1)
        self.distance = 0
        self.voltage = 0
        self.capacity = 0
        self.started_robot=False

        self.direction = 18
        self.speed = 13

        self.all_time_max = 90
        self.max_speed = 70
        self.min_speed = 25
        self.current_speed = 70

        self.GPIO_TRIGGER = 23
        self.GPIO_ECHO = 24
        GPIO_RED = 16
        GPIO_GREEN = 20

        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.GPIO_TRIGGER, GPIO.OUT)
        GPIO.setup(self.GPIO_ECHO, GPIO.IN)
        GPIO.setup(self.speed, GPIO.OUT)
        GPIO.setup(GPIO_RED, GPIO.OUT)
        GPIO.setup(GPIO_GREEN, GPIO.OUT)

        self.pwm_speed = GPIO.PWM(self.speed, 100)
        self.IPAdresse = os.popen('hostname -I').readlines()
        GPIO.output(GPIO_RED, GPIO.HIGH)
        GPIO.output(GPIO_GREEN, GPIO.HIGH)

        self.serial = i2c(port=1, address=0x3c)
        self.timer = threading.Timer(2.0, self.drawdisplay)
        self.started_robot = False
        atexit.register(self.stopme)
        
    def getDistance(self):
      GPIO.output(self.GPIO_TRIGGER, True)
      # setze Trigger nach 0.01ms aus LOW
      time.sleep(0.00001)
      GPIO.output(self.GPIO_TRIGGER, False)

      StartZeit = time.time()
      StopZeit = time.time()

      # speichere Startzeit
      while GPIO.input(self.GPIO_ECHO) == 0:
        StartZeit = time.time()  
        if abs(StopZeit-StartZeit) >= 0.5:
            break
       
       # speichere Ankunftszeit
      while GPIO.input(self.GPIO_ECHO) == 1:
        StopZeit = time.time()

      # Zeit Differenz zwischen Start und Ankunft
      TimeElapsed = StopZeit - StartZeit
      # mit der Schallgeschwindigkeit (34300 cm/s) multiplizieren
      # und durch 2 teilen, da hin und zurueck
      self.distance = (TimeElapsed * 34300) / 2

    def readVoltage(self):
      address = 0x36
      read = self.bus.read_word_data(address, 2)
      swapped = struct.unpack("<H", struct.pack(">H", read))[0]
      self.voltage = swapped * 78.125 /1000000

    def readCapacity(self):
      address = 0x36
      read = self.bus.read_word_data(address, 4)
      swapped = struct.unpack("<H", struct.pack(">H", read))[0]
      self.capacity = swapped/256

    def inc_max_speed(self):
      # set  max speed ++
      if self.max_speed < self.all_time_max:
        self.max_speed +=1

    def dec_max_speed(self):
      # set max speed --
      if self.max_speed > 0:
        self.max_speed -=1

    def accelerate(self):
      if self.current_speed < self.max_speed:
        self.current_speed += 5
        self.pwm_speed.start(self.current_speed)

    def slowdown(self):
      if self.current_speed > (self.min_speed + 10):
        self.current_speed -= 10
        self.pwm_speed.start(self.current_speed)

    def exposed_start_up(self):      
     self.started_robot = True 
     if self.started_robot:
        helper = False
        print("helper false")
        self.timer.start()
        self.pwm_speed.start(self.current_speed)
        self.running = True

        while True:
          if not self.timer.is_alive:
              print("starting timer")
              #self.timer.start()
          self.getDistance()
          if self.distance <= 60:
            self.pwm_speed.stop()
            self.current_speed = 0
            GPIO.output(self.speed, GPIO.LOW)
            self.running = False
          if self.distance >= 60 and self.running == False:
            self.current_speed = 40
            self.pwm_speed.start(self.current_speed)
            self.running = True
          if self.distance >= 100 and self.running == True:
            self.accelerate()
            self.running = True
          if self.distance < 80 and self.running == True:
            self.slowdown()
            self.running = True
          print(self.current_speed)
          time.sleep(0.1)

    def drawdisplay(self):
       device = sh1106(self.serial) #get_device()
       print("Hello")
       with canvas(device) as draw:
          # First define some constants to allow easy resizing of shapes.
          top = 2 
          # Write two lines of text.
          size = draw.textsize('World!')
          draw.text((0, top + 4), 'Voltage:%5.2fV:  ' % self.voltage, fill="white")
          draw.text((0, top + 16), 'Battery:%5i%%' % self.capacity, fill="white")
          draw.text((0, top + 28), 'Abstand:%5.2fcm' % self.distance, fill="white")
          draw.text((0, top + 40), 'IP: '+ ' '.join(self.IPAdresse), fill="white")
       self.timer = threading.Timer(2.0, self.drawdisplay)
       self.timer.start()
 
    def stopme(self):
      self.pwm_speed.stop()
      #TODO Bildschirm loeschen
      GPIO.output(self.speed, GPIO.LOW)
      print("Clean up bitch")
      GPIO.cleanup()

    def get_info(self):
        self.info = [self.current_speed,self.max_speed,self.distance,self.capacity,self.voltage]
        return self.info

if __name__ == "__main__":
    server = ThreadedServer(AutonomousCarService, port = 18812, protocol_config={
        'allow_public_attrs': True, })
    server.start()


